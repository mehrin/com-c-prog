#include<iostream>
using namespace std;

int main(){
    int myNumbers[] = {98, 95, 43, 28, 57, 85, 8, 16, 100, 36};

    for(int i = 0; i < 10; i++){
        int currentShortest = myNumbers[i];
        int currentShortestPosition = i;

        for(int j = i+1; j<10; j++){
            if(currentShortest > myNumbers[j]){
                currentShortest = myNumbers[j];
                currentShortestPosition = j;
            }
        }

        int temp = myNumbers[i];
        myNumbers[i] = currentShortest;
        myNumbers[currentShortestPosition] = temp;
    }

    for(int i = 0; i<10; i++){
        cout<<myNumbers[i]<<" ";
    }

    return 0;
}
