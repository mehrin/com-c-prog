/*
55
1 & 55
2...x-1

55%2 == 0 ??? false;
55%3 == 0 ??? false;
55%4 == 0 ??? false;
55%5 == 0 ??? true;

55 is not prime

7
2...6
7%2 == 0 ? no
7%3 == 0 ? no
7%4 == 0 ? no
7%5 == 0 ? no
7%6 == 0 ? no
7 is prime
*/

#include<iostream>
using namespace std;

int main(){
    int theNumber;
    bool isPrime = true;
    cout<<"Enter Number: ";
    cin>>theNumber;
    for(int i = 2; i<theNumber; i++){
        if(theNumber % i == 0){
            isPrime = false;
            break;
        }
    }

    if(isPrime){
        cout<<"This is a prime number";
    }else{
        cout<<"Not prime number";
    }
    return 0;
}
