/*
a b c d e f
0 1 2 3 4 5
*/

#include<iostream>
using namespace std;

int main(){
    char *myString = "abcdef";
    int i = 0;

    while(myString[i] != '\0'){
        i++;
    }
    cout<<"String Length: "<<i<<endl;

    for(i=i-1; i>=0; i--){
        cout<<myString[i];
    }

    //cout<<"Reverse of abcde is edcba."<<endl;
    return 0;
}
