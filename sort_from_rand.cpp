#include<iostream>
#include<stdlib.h>
using namespace std;

int main(){
    int range = 10, theArray[999], summation = 0;

    /*
    cout<<"Enter size of the array (between 1 to 999): ";
    cin>>range;
    */

    // generate the array to sort with random integer
    for(int i = 0; i < range; i++){
        theArray[i] = rand() % 100;
        summation += theArray[i];
    }

    // sort the array
    for(int i = 0; i < range; i++){
        for(int j = 0; j<range-i-1; j++){
            if(theArray[j] > theArray[j+1]){
                int temp = theArray[j];
                theArray[j] = theArray[j+1];
                theArray[j+1] = temp;
            }
        }
    }

    // print the array
    for(int i = 0; i < range; i++){
        cout<<theArray[i]<<" ";
    }
    cout<<endl<<"The Summation is: "<<summation;
    return 0;
}
