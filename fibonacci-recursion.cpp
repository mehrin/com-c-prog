#include<iostream>
using namespace std;

/*
 f(n) = f(n-1) + f(n-2)
 f(0) = 0;
 f(1) = 1

 fibonacci(5) = fibonacci(4) + fibonacci(3)
 fibonacci(4) = fibonacci(3) + fibonacci(2)
 fibonacci(3) = fibonacci(2) + fibonacci(1)
 fibonacci(2) = fibonacci(1) + fibonacci(0)
*/

int fibonacci(int n){
    if(n == 0){
        return 0;
    }else if( n == 1 ){
        return 1;
    }else{
        return fibonacci(n-1) + fibonacci(n-2);
    }
}

int main(){
    cout<<fibonacci(5);
    return 0;
}
