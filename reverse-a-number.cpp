/*
15324
15324 % 10 = 4.... int (15324/10) = 1532
1532 % 10 = 2 ... int (1532/10) = 153
153 % 10 = 3 .... int (153/10)=15
15 % 10 = 5 .... int (15/10)=1
1 % 10 = 1 .... int (1/10)=0
*/

#include<iostream>
using namespace std;

int main(){
    int theNumber = 12345;
    int theNumberCopy = theNumber;
    int reverseNumber = 0;
    while(true){
        int theDigit = theNumberCopy % 10;
        reverseNumber = reverseNumber * 10 + theDigit;
        theNumberCopy = theNumberCopy/10;
        if(theNumberCopy < 1){
            break;
        }
    }

    cout<<"Reverse of "<<theNumber<<" is "<<reverseNumber<<"."<<endl;
    return 0;
}
