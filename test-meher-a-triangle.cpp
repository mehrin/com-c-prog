/*
a           l = 0
..a         l = 1, s = 2
....a       l = 2, s = 4
..a         l = 1, s = 2
a           l = 0
*/
#include<iostream>
using namespace std;
int main(){
    int line = 0;
    for( ;line<3; line++){
        for(int spaces = 0; spaces <= line*2; spaces++){
            cout<<" ";
        }
        cout<<"a"<<endl;
    }
    line -= 2;
    for( line = 1;line>=0; line--){
        for(int spaces = 0; spaces <= line*2; spaces++){
            cout<<" ";
        }
        cout<<"a"<<endl;
    }

    return 0;
}
