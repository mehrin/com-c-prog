/*
f(0) = 0
f(1) = 1
f(n) = f(n-1) + f(n-2)
*/

#include<iostream>
using namespace std;

int main(){
    int f1, f2, fn;
    int summation=0;
    for(int i = 0; i <=10; i++){
        if(i==0){
            f1 = 0;
            fn = 0;
        }
        else if(i==1){
            f2 = 1;
            fn = 1;
        }
        else{
            fn=f1+f2;
            f1=f2;
            f2=fn;
            summation+=fn;
        }
        if(i==8){
            cout<<fn<<endl;
        }
    }
    cout<<summation;
    return 0;
}
