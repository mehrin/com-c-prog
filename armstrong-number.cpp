#include<iostream>
using namespace std;

/*
! This program computes all Armstrong numbers in the range of
! 0 and 999.  An Armstrong number is a number such that the sum
! of its digits raised to the third power is equal to the number
! itself.  For example, 371 is an Armstrong number, since
! 3**3 + 7**3 + 1**3 = 371.
*/

int main(){
    int counter = 0;

    for(int a = 0; a <= 9; a++){
        for(int b = 0; b <= 9; b++){
            for(int c = 0; c <= 9; c++){
                int abc = a*100 + b*10 + c;
                int a3b3c3 = a*a*a + b*b*b + c*c*c;

                if(abc == a3b3c3){
                    cout<<"Armstrong Number#" << counter << ": "<<abc<<endl;
                }
            }
        }
    }

    return 0;
}
