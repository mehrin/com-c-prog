#include<iostream>
using namespace std;
/*
1^k + 2^k + 3^k .... n^k

k = 3
i = 1... 1*1*1 = 1
i = 2... 2*2*2 = 8
i = 3... 3*3*3 = 27
product=i^K;

*/
int main(){
    int n,k;
    cout<<"n:";
    cin>>n;
    cout<<"k";
    cin>>k;
    int summation=0;
    for(int i=1;i<=n;i++){
        int product=1;
        for(int j = 1; j<= k; j++){
            product *= i;
        }
        cout<<product<<endl;

        summation += product;
    }
    cout<<summation;
    return 0;
}

