/*
1# 6
2# 28
3# 496
4# 8128
*/

#include<iostream>
using namespace std;

int main(){

    int maxRange;
    cout<<"Enter Max Range: ";
    cin>>maxRange;

    int counter = 1;

    for(int theNumber = 1; theNumber <= maxRange; theNumber++){
        int summation = 0;
        for(int i = 1; i<theNumber; i++){
            if(theNumber % i == 0){
                summation += i;
            }
        }
        if(theNumber == summation){
            cout<<counter<<"# "<<theNumber<<endl;
            counter++;
        }
    }

    return 0;
}
