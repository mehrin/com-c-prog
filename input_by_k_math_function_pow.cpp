#include<iostream>
#include<math.h>
using namespace std;
/*
1^k + 2^k + 3^k .... n^k

k = 3
i = 1... 1*1*1 = 1
i = 2... 2*2*2 = 8
i = 3... 3*3*3 = 27
product=i^K;

*/
int main(){
    int n,k;
    cout<<"n:";
    cin>>n;
    cout<<"k";
    cin>>k;
    float summation=0;
    for(int i=1;i<=n;i++){
        summation += pow(i, k);
    }
    cout<<"Summation: "<<summation<<endl;
    cout<<"Avg: "<<summation/7<<endl;
    cout<<"Avg (ceil): "<<ceil(summation/7)<<endl;
    cout<<"Avg (floor): "<<floor(summation/7)<<endl;
    return 0;
}

