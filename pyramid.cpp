/*
     *
    ***
   *****
  *******
 *********
***********

.....*
....***
...*****
..*******
.*********
***********

totalLine - currentLineNumber
6-1
6-2
6-3
.
.
6-6
*/
#include<iostream>
using namespace std;

int main(){
    int total_line;
    cout<<"Total line";
    cin>>total_line;
    for(int line=1;line<=total_line;line++){
        for(int space=1;space<=total_line-line;space++){
            cout<<" ";
        }
        for(int star=1;star<=2*line-1;star++){
            cout<<"*";
        }
        cout<<endl;
    }

    return 0;
}
