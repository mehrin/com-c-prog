/* insertion sort ascending order */

#include <iostream>
using namespace std;

int main()
{
  int n = 10, array[1000] = {4, 1, 3, 5, 10, 7, 8, 6, 2}, c, d, t;

  for (int c = 1 ; c <= n - 1; c++) {
    d = c;

    while ( d > 0 && array[d] < array[d-1]) {
      t = array[d];
      array[d]   = array[d-1];
      array[d-1] = t;
      d--;
    }
  }

  cout<<"Sorted list in ascending order:\n";

  for (int c = 0; c <= n - 1; c++) {
    cout<<array[c]<<" ";
  }

  return 0;
}
