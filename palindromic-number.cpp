#include<iostream>
using namespace std;


int main(){
    int counter = 1;

    for(int myNumber = 1; myNumber<=999; myNumber++){
        int myNum = myNumber;
        int reverseNumber = 0;
        while(true){
            reverseNumber = reverseNumber*10 + myNum % 10;

            myNum = myNum/10;
            if(myNum < 1){
                break;
            }
        }
        if(myNumber == reverseNumber){
            cout<<"Palindromic# "<<counter<<": "<<myNumber<<endl;
            counter++;
        }
    }

    return 0;
}
