/*
1---- 1000
*/

#include<iostream>
using namespace std;

int main(){
    int theNumber;

    for(int theNumber = 1; theNumber <= 1000; theNumber++){
        bool isPrime = true;
        for(int i = 2; i<theNumber; i++){
            if(theNumber % i == 0){
                isPrime = false;
                break;
            }
        }

        if(isPrime){
            cout<<theNumber<<", ";
        }
    }
    return 0;
}
