/*
ASC bubble sort
3 1 5 6 2 4 7 8
1 3 5 6 2 4 7 8
1 3 5 2 6 4 7 8
1 3 5 2 4 6 7 8

*/

#include<iostream>
using namespace std;

int main(){
    int myNumbers[8] = {3, 1, 5, 6, 2, 4, 7, 8};

    for(int i = 0; i<8; i++){
        for(int j = 0; j<7-i; j++){
            if(myNumbers[j] > myNumbers[j+1]){
                // swap
                int temp = myNumbers[j];
                myNumbers[j] = myNumbers[j+1];
                myNumbers[j+1] = temp;
            }
        }
    }
    for(int i = 0; i<8; i++){
        cout<<myNumbers[i]<<" ";
    }
    return 0;
}
