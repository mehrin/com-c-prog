#include<iostream>
using namespace std;

int gcd (int bn, int sn){
    int reminder;
    while(reminder != 0){
        reminder = bn % sn;
        bn = sn;
        sn = reminder;
    }

    return  bn;
}

int main(){
    int bn = 76, sn = 36;
    cout<<gcd(bn, sn);
    return 0;
}
