#include<iostream>
#include<string.h>

using namespace std;

class Vehicle{
protected:
    string cc;
    string menufacturer;
    string weight;

public:
    Vehicle(){
        cout<<"Empty vehicle created.\n";
    }
    Vehicle(string cc, string weight, string menufacturer){
        this->cc = cc;
        this->weight = weight;
        this->menufacturer = menufacturer;
    }
	set_cc(string cc){
	    this->cc = cc;
	}
	string get_cc(){
        return this->cc;
	}

	set_menufacturer(string menufacturer){
	    this->menufacturer=menufacturer;
	}
	string get_menufacturer(){
        return this->menufacturer;
	}

	set_weight(string weight){
	    this->weight=weight;
	}
	string get_weight(){
        return this->weight;
	}
};

class Two_wheeler:public Vehicle{
public:
    const string vehicle_type = "Motorcycle";
    Two_wheeler(string cc, string weight, string menufacturer):Vehicle(cc, weight, menufacturer){
        cout<<"Two wheeler created.\n";
    }

    void print_details(){
        cout<<"-------------------"<<endl;
        cout<<"Vehicle Type: "<<Two_wheeler::vehicle_type<<endl;
        cout<<"CC: "<<Vehicle::get_cc()<<endl;
        cout<<"Manufacturer: "<<Vehicle::get_menufacturer()<<endl;
        cout<<"Weight: "<<Vehicle::get_weight()<<endl;
        cout<<"-------------------"<<endl;
    }
};


class Four_wheeler:public Vehicle{
protected:
    int seats;
    string route;
public:
    const string vehicle_type = "Chair Chakka";
    Four_wheeler(string cc, string weight, string menufacturer, int seats, string route):Vehicle(cc, weight, menufacturer){
        this->seats = seats;
        this->route = route;
        cout<<"Four wheeler created.\n";
    }

    void print_details(){
        cout<<"-------------------"<<endl;
        cout<<"Vehicle Type: "<<Four_wheeler::vehicle_type<<endl;
        cout<<"CC: "<<Vehicle::get_cc()<<endl;
        cout<<"Manufacturer: "<<Vehicle::get_menufacturer()<<endl;
        cout<<"Weight: "<<Vehicle::get_weight()<<endl;
        cout<<"Route: "<<this->route<<endl;
        cout<<"Seats: "<<this->seats<<endl;
        cout<<"-------------------"<<endl;
    }
};


int main(){
    Two_wheeler motorCycle("150cc", "180kg", "BMW");
    motorCycle.print_details();
    Two_wheeler r6("600cc", "210kg", "Yamaha R-6");
    r6.print_details();

    Four_wheeler a_bus("2000cc", "1000kg", "Hino", 27, "Dhk-Bsl");
    a_bus.print_details();

	return 0;
}
