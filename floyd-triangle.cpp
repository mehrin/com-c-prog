/*
1
2 3
4 5 6
7 8 9 10
*/
#include<iostream>
using namespace std;
int main(){
    int totalLine = 4;
    int counter = 1;

    for(int line=0; line<totalLine; line++){
        for(int number = 0;number<=line;number++){
            cout<<counter++<<" ";
        }
        cout<<endl;
    }

    return 0;
}

