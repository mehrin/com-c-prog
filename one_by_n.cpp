/*1/1 + 1/2 + 1/3 + ...... + 1/n =*/
#include<iostream>
#include<math.h>
using namespace std;

int main(){
    int n;
    cout<<"n:";
    cin>>n;
    float summation=0;
    for(int i=1;i<=n;i++){
        float div= (float) 1/i;
        summation+=div;
    }

    cout<<summation<<endl<<ceil(summation);
    return 0;
}

