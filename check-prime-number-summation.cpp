#include<iostream>
using namespace std;

int main(){
    int summation = 0;
    for(int theNumber = 1; theNumber < 100; theNumber++){
        bool isPrime = true;
        for(int i = 2; i<theNumber; i++){
            if(theNumber % i == 0){
                isPrime = false;
                break;
            }
        }
        if(isPrime){
            summation += theNumber;
        }
    }
    cout<<"Summation of prime numbers: "<<summation;

    return 0;
}
